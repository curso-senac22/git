<?php

use App\Http\Controllers\Admin\UsuariosController;
use App\Http\Controllers\Admin\AdminController;

use App\Http\Controllers\LoginController;

use App\Http\Controllers\HomeController;
use Illuminate\Routing\Route as RoutingRoute;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('/',[HomeController::class, 'home'])->name('jong');
    Route::get('/ongs',[HomeController::class, 'ongs'])->name('jong.ongs');
    Route::get('/causa',[HomeController::class, 'causa'])->name('jong.caus');
    Route::get('/doacao',[HomeController::class, 'doacao'])->name('jong.doacao');
    Route::get('/cadastrar',[HomeController::class, 'cadastrar'])->name('jong.cadastrar');
    Route::get('/pet',[HomeController::class, 'pet'])->name('jong.saudedoauau');
    Route::get('/pet/saude-miau',[HomeController::class, 'petMiau'])->name('jong.saudemiau');
    Route::get('/pet/petShop',[HomeController::class, 'petShop'])->name('jong.petShop');
    Route::get('/pet/visualizarpet',[HomeController::class, 'visualizarpet'])->name('jong.visualizarpet');
    Route::get('/conheca',[HomeController::class,'conheca'])->name('jong.conheca');
    Route::get('/voluntarios',[HomeController::class,'voluntarios'])->name('jong.voluntarios');
    Route::get('/login',[LoginController::class,'index'])->name('jong.login');
    Route::post('/login',[LoginController::class,'postLogin'])->name('jong.postLogin');
    Route::post('/logout();',[LoginController::class,'logout'])->name('jong.logout');



    Route::prefix('/admin')->middleware('auth')->group(function(){

        Route::get('/', [AdminController::class, 'home'])->name('admin.home.index');

        Route::get('/usuarios', [UsuariosController::class, 'index'])->name('admin.usuarios.index');
        Route::get('/cadastrar', [UsuariosController::class, 'create'])->name('admin.usuarios.cadastrar');
        Route::post('/cadastrar', [UsuariosController::class, 'store'])->name('admin.usuarios.cadastrar');
        Route::get('/editar/{id}', [UsuariosController::class, 'edit'])->name('admin.usuarios.editar');
        Route::put('/editar/{id}', [UsuariosController::class, 'update'])->name('admin.usuarios.editar');
        Route::delete('/deletar/{id}', [UsuariosController::class, 'destroy'])->name('admin.usuarios.deletar');


});

@extends('layout.site')

@section('titulo', 'Visualizar Pets')

@section('conteudo')



<div id="tituloPagina">
    <h2>Adote-Me</h2>
</div>
<div class="container">
    <div class="row">
        <div id="texxx" class="col-md-4">
            <img src="/img/petpet1.jpg" class="img-fluid" width="400px" alt="adotePet">


        </div>
        <div id="tex" class="col-md-8">
            <strong>
                <p>Nome:
            </strong> Russo</p>
            <strong>
                <p>Idade:
            </strong> 1 ano</p>
            <strong>
                <p>Historia:
            </strong> Quando eu era pequenininho, meu antigo tutor não me quis mais por eu ser assustado e
            tímido, então me entregou para adoção e o pessoal no Instituto me acolheu. Passei muito tempo
            inseguro demais, sem querer confiar em ninguém... cheguei até a ser adotada por uma família que não
            teve paciência e respeito para entender tudo o que passei e acabaram me devolvendo para o abrigo.
            Hoje em dia estou melhor, gosto de pessoas e carinho, moro com outros cães no canil e me dou super
            bem com eles, só preciso de um pouquinho de paciência no início de amizades novas e assim que
            acostumo eu me torno muito carinhosa.</p>
            <strong>
                <p>Ong:
            </strong>Amor Animal</p>

        </div>


    </div>
</div>

@endsection

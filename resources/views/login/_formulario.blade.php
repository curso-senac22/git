@csrf



@if ($errors->any())

    @foreach ($errors->all() as $error)
        <div>
            {{ $error }}
        </div>
    @endforeach

@endif

<div class="col-md-12">
    <label for="nome" class="form-label">Nome</label>
    <input type="text" class="form-control @error('nome') is-invalid @enderror" name="nome" id="nome" placeholder="Insira o Nome" value="{{ old('nome', $usuario->nome) }}">

@error('nome')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
@enderror

</div>
<div class="col-md-12">
    <label for="email" class="form-label">E-mail</label>
    <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Insira a E-mail" value="{{ old('email', $usuario->email) }}">

@error('email')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
@enderror

<div class="col-md-12">
    <label for="perfil" class="form-label">Perfil</label>
    <input type="text" class="form-control @error('perfil') is-invalid @enderror" name="perfil" id="perfil" placeholder="Insira deu perfil" value="{{ old('perfil', $usuario->perfil) }}">

@error('perfil')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
@enderror

<div class="col-md-12">
    <label for="funcao" class="form-label">Função</label>
    <input type="text" class="form-control @error('funcao') is-invalid @enderror" name="funcao" id="funcao" placeholder="Insira a função" value="{{ old('funcao', $usuario->funcao) }}">

@error('funcao')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
@enderror

<div class="col-md-12">
    <label for="telefone" class="form-label">Telefone</label>
    <input type="text" class="form-control @error('telefone') is-invalid @enderror" name="telefone" id="telefone" placeholder="Insira o Telefone" value="{{ old('telefone', $usuario->teçefone) }}">

@error('telefone')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
@enderror

<div class="col-md-12">
    <label for="cidade" class="form-label">Cidade</label>
    <input type="text" class="form-control @error('cidade') is-invalid @enderror" name="cidade" id="nome" placeholder="Insira a Cidade" value="{{ old('cidade', $usuario->cidade) }}">

@error('cidade')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
@enderror

<div class="col-md-12">
    <label for="cep" class="form-label">Cep</label>
    <input type="text" class="form-control @error('cep') is-invalid @enderror" name="cep" id="cep" placeholder="Insira o CEP" value="{{ old('cep', $usuario->cep) }}">

@error('cep')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
@enderror

<div class="col-md-12">
    <label for="complemento" class="form-label">Complemento</label>
    <input type="text" class="form-control @error('complemento') is-invalid @enderror" name="complemento" id="nome" placeholder="Insira o Complemento" value="{{ old('complemento', $usuario->complemento) }}">

@error('complemento')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
@enderror

<div class="col-md-12">
    <label for="bairro" class="form-label">Bairro</label>
    <input type="text" class="form-control @error('bairro') is-invalid @enderror" name="bairro" id="bairro" placeholder="Insira o Bairro" value="{{ old('bairro', $usuario->bairro) }}">

@error('bairro')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
@enderror

<div class="col-md-12">
    <label for="estato" class="form-label">Estado</label>
    <input type="text" class="form-control @error('estado') is-invalid @enderror" name="estado" id="estado" placeholder="Insira o Estado" value="{{ old('estado', $usuario->estado) }}">

@error('estado')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
@enderror

<div class="col-md-12">
    <label for="responsavel" class="form-label">Responsavel</label>
    <input type="text" class="form-control @error('responsavel') is-invalid @enderror" name="responsavel" id="responsavel" placeholder="Insira o Responsavel" value="{{ old('responsavel', $usuario->reponsavel) }}">

@error('responsavel')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
@enderror

<div class="col-md-12">
    <label for="cpf" class="form-label">CPF</label>
    <input type="text" class="form-control @error('cpf') is-invalid @enderror" name="cpf" id="cpf" placeholder="Insira o seu Cpf" value="{{ old('cpf', $usuario->cpf) }}">

@error('cpf')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
@enderror

<div class="col-md-12">
    <label for="tipo" class="form-label">Tipo</label>
    <input type="text" class="form-control @error('tipo') is-invalid @enderror" name="tipo" id="tipo" placeholder="Insira o seu tipo" value="{{ old('tipo', $usuario->tipo) }}">

@error('tipo')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
@enderror


</div>
<div class="col-md-12">
    <label for="senha" class="form-label">Senha</label>
    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="senha" placeholder="">

    @error('password')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror
</div>

<div class="col-12">
    <button type="submit" class="btn btn-primary">Salvar</button>
</div>

@extends('layout.site')

@section('conteudo')

<div class="container" >
    <form action="{{route('jong.postLogin')}}" method="POST">
        @csrf
        <div class="text-center">
            <img class="mb-4" src="images/logo.png" alt="">
            <h1 class="h3 mb-3 fw-normal">Acessar</h1>
        </div>
        <div class="col-md-12">
            <div class="form-floating">
                <input type="email" class="form-control" id="floatingInput" placeholder="name@example.com" name="email">
                <label for="floatingInput">E-mail</label>
            </div>
        </div>
        <div class="form-floating">
            <input type="password" class="form-control" id="floatingPassword" placeholder="Password" name="password">
            <label for="floatingPassword">Senha</label>
        </div>

        <div class="checkbox mb-3">
            <label>
                <input type="checkbox" value="remember-me"> Lembrar-me
            </label>
        </div>

        <button class="w-100 btn btn-lg btn-primary" type="submit">Entrar</button>

    </form>
</div>

@endsection

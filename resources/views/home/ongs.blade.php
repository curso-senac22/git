@extends('layout.site')

@section('titulo', 'Ongs')

@section('conteudo')

<div id="tituloPagina">
    <h2>Nossos Parceiros</h2>
</div>

<section>

        <div class="container">

            <div class="row justify-content-md-center ">
                <div class="col-md-3 col-6 mt-4">
                    <img src="img/1.png" class="img-fluid" alt="doação ong amor animal">
                </div>

                <div class="col-md-3 col-6 mt-4">
                    <img src="img/garra.png" class="img-fluid" alt="doação ong garra">
                </div>
            </div>
        </div>

</section>


@endsection

@extends('layout.site')

@section('titulo', 'voluntarios')

@section('conteudo')

<div class="container" >

<div class="container">

    <div class="row">
        <div id="tex" class="col-md-6">
            <img src="img/renata.jpg" class="img-fluid" width="400px" alt="voluntaria">
        </div>
        <div id="texs" class="col-md-6">

            <p>Renata: Tenho 24 anos, decidi ajudar o projeto
                pois senti a necessidade de acrescentar em algo nas ongs animais da minha cidade.
                Peguei meu tempo livre e tornei em amor. Foi a melhor decisão que já tive! </p>

        </div>

        <div class="row">
             <div id="tex" class="col-md-6">
                <img src="img/ricardo.jpg" class="img-fluid" width="400px" alt="voluntario">
            </div>
            <div id="texs" class="col-md-6">
                <p>Ricardo: Sou o Rick, 30 anos. Acompanho o projeto a vários anos, e quando
                    tive a oportunidade e coragem, decidi me juntar. Toda a recepção e tudo
                    que fazemos é muito lindo. Somos unidos a um proposito, mudar a vida de
                    pessoas e animais através da adoção.</p>
            </div>
        </div>
    </div>
</div>

@endsection

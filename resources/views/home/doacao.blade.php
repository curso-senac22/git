@extends('layout.site')

@section('titulo', 'Faça sua Doação')

@section('conteudo')

<div id="tituloPagina">
    <h2>Doação</h2>
</div>

<section>
    <div id="juncaoOngs">

        <div class="container">

            <div class="row justify-content-md-center">
                <div class="col-md-5 col-6">
                    <img src="img/doacao1.png" class="img-fluid" alt="doação ong amor animal">
                </div>

                <div class="col-md-5 col-6">
                    <img src="img/doacao2.png" class="img-fluid" alt="doação ong garra">
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

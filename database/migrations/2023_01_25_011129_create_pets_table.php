<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pets', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('nome',45);
            $table->string('raca');
            $table->string('cor');
            $table->string('descricao');
            $table->string('idade',45);
            $table->string('sexo',6);
            $table->boolean('animais_adotados',45);
            $table->boolean('animais_resgatados',45);
            $table->boolean('animais_castrados',45);
            $table->boolean('animais_vacinados',45);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pets');
    }
};

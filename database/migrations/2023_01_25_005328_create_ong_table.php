<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ongs', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('cnpj', 45);
            $table->string('telefone', 16);
            $table->string('email',50);
            $table->string('endereco', 100);
            $table->string('CEP',45);
            $table->string('complemento',45);
            $table->string('bairro',45);
            $table->string('cidade', 45);
            $table->string('estado',45);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ong');
    }
};

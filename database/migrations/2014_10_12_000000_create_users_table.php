<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->id();

            $table->string('nome', 80);
            $table->string('telefone', 16);
            $table->string('email',50);
            $table->string('responsavel', 50);
            $table->string('cpf',45);
            $table->string('perfil', 45)->default('voluntario');
            $table->string('endereco', 100);
            $table->string('CEP',45);
            $table->string('complemento',45);
            $table->string('bairro',45);
            $table->string('cidade', 45);
            $table->string('estado',45);
            $table->text('funcao');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};


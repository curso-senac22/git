<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(){
       return view('home.index');
    }

    public function ongs(){
        return view('home.ongs');
    }

    public function causa(){
        return view('home.causa');
    }

    public function doacao(){
        return view('home.doacao');
    }

    public function pet (){
        return view('pet.index');
    }

    public function petMiau (){
        return view('pet.saudeMiau');
    }

    public function petShop (){
        return view('pet.petShop');
    }

    public function visualizarpet(){
        return view('pet.visualizarpet');
    }

    public function conheca(){
        return view('home.conheca');
    }

    public function voluntarios(){
        return view('home.voluntarios');
    }


}

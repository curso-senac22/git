<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(){
        return view('login.login');
    }


    public function create()
    {
        return view('login.cadastrar',[
            'usuario'=> new User()
        ]);
    }

    public function store(Request $request)
    {

        dd($request);
        $request->validate([
            'nome'=>'required',
            'email'=>'required|email|unique:users',
            'telefone'=>'required',
            'responsavel'=>'required',
            'cpf'=>'required',
            'endereco'=>'required',
            'cep'=>'required',
            'complemento'=>'required',
            'bairro'=>'required',
            'cidade'=>'required',
            'estado'=>'required',
            'funcao'=>'required',
            'password'=>'required|min:8'
        ]);

        try {

            $usuario = new User();
            $usuario->nome = $request->nome;
            $usuario->email = $request->email;
            $usuario->password = bcrypt($request->password);
            $usuario->perfil = $request->perfil;
            $usuario->funcao = $request->funcao;
            $usuario->telefone = $request->telefone;
            $usuario->cidade = $request->cidade;
            $usuario->cep = $request->cep;
            $usuario->endereco = $request->endereco;
            $usuario->complemento = $request->complemento;
            $usuario->bairro = $request->bairro;
            $usuario->estado = $request->estado;
            $usuario->responsavel = $request->responsavel;
            $usuario->cpf = $request->cpf;
            $usuario->tipo = $request->tipo;


            $usuario->save();

            return redirect()->route('admin.usuarios.index')->with('sucesso', 'Usuário cadastrado com sucesso');
        } catch (\Exception $e) {

            //dd($e->getMessage());

            return redirect()->back()->withInput()->with('erro', 'Ocorreu um erro ao cadastrar, por favor tente novamente');
        }
    }

    public function postLogin(Request $request)
    {


        if (Auth::attempt(['email' => $request->email, 'password' =>  $request->password])) {


           return redirect()->route('admin.home.index');

        } else {
            return redirect()->back()->withInput()->with('error', 'E-mail ou Senha inválido!');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}




